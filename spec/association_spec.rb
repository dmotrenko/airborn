require 'airborne'
require 'config'


describe 'association' do

  it 'should validate association' do
    get '/association/1' 
    expect_json_types('Category', 
    	{Id: :int,
        CategoryTypeId: :int,
        ExtraCategoryName: :string
        })
    expect_json_types( 
        IsTestMode: :bool,
        OnlinePaymentAccount: :string,
        DeletedDate: :date,
        DisableEditMemberData: :bool,
        ReferralCode: :string,
        ReferrerAssociationId: :int 
        )
    expect_json(
        ReferralCode: regex(".....") # regex support 
    )
    end
end

