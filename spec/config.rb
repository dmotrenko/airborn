Airborne.configure do |config|
  config.base_url = 'http://env5:8020'
  config.headers = { 'X-ApplicationId' => 'Swagger', 'X-ApiVersion' => 1.0, 'Accept' => 'application/json; charset=utf-8' }
end