ruby 2.3.1p112 (2016-04-26 revision 54768) [i386-mingw32]
gem --version 2.6.7

windows instalation for airborne

Install ruby
http://dl.bintray.com/oneclick/rubyinstaller/rubyinstaller-2.3.1.exe

Unfortunatelly, there is SSL bug that does not allow to install gems. To fix this bug, do the following:
1. Download https://rubygems.org/downloads/rubygems-update-2.6.7.gem

2. gem install --local rubygems-update-2.6.7.gem

3. update_rubygems --no-ri --no-rdoc

4. Verify gem 2.6.7 is installed

   gem --version
   
5. gem uninstall rubygems-update -x


Now gem installation should work
 6. gem install airborne

To run tests, cd to project folder and run
rspec specs


ruby 2.3.1p112 (2016-04-26 revision 54768) [i386-mingw32]
gem --version 2.6.7